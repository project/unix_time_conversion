<?php

namespace Drupal\unix_time_conversion\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class UnixTimeConversionSettings.
 *
 * @package Drupal\unix_time_conversion\Form\UnixTimeConversionSettings
 */
class UnixTimeConversionSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'unix_time_conversion.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'unix_time_conversion_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    $config = \Drupal::configFactory()->getEditable('unix_time_conversion.settings');
    // Include the helper functions file.
    module_load_include('inc', 'unix_time_conversion', 'unix_time_conversion.helper_functions');
    $form = [];
    /*
     * Timestamp To Date.
     */
    $form['unix_time_conversion_time_to_date'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Timestamp To Date'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    // Timestamp field title.
    $form['unix_time_conversion_time_to_date']['unix_time_conversion_timestamp_field_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp Field Title'),
      '#description' => $this->t('Will serve as field title for input timestamp field. Ex: Timestamp.'),
      '#required' => TRUE,
      '#default_value' => $config->get('unix_time_conversion_timestamp_field_title'),
    ];
    // Timestamp field description.
    $form['unix_time_conversion_time_to_date']['unix_time_conversion_timestamp_field_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timestamp Field Description'),
      '#description' => $this->t('Will serve as field description for input timestamp field.'),
      '#default_value' => $config->get('unix_time_conversion_timestamp_field_description'),
    ];
    $url = Url::fromUri('http://php.net/manual/en/function.date.php');
    $link_options = ['attributes' => ['target' => '_blank']];
    $url->setOptions($link_options);
    $link = Link::fromTextAndUrl($this->t('php manual'), $url)->toString();

    // Output format.
    $form['unix_time_conversion_time_to_date']['unix_time_conversion_time_to_date_output_format'] = [
      '#title' => $this->t('Date Output Format'),
      '#type' => 'textfield',
      '#description' => $this->t('A user-defined date format. See the @phpmanual for available options.', [
        '@phpmanual' => $link,
          ]
      ),
      '#required' => TRUE,
      '#default_value' => $config->get('unix_time_conversion_time_to_date_output_format'),
    ];

    // Date To Timestamp.

    $form['unix_time_conversion_date_to_time'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Date To Timestamp'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    // Date field title.
    $form['unix_time_conversion_date_to_time']['unix_time_conversion_date_field_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date Field Title'),
      '#description' => $this->t('Will serve as field title for input date field. Ex: Date.'),
      '#required' => TRUE,
      '#default_value' => $config->get('unix_time_conversion_date_field_title'),
    ];

    // Time field title.
    $form['unix_time_conversion_date_to_time']['unix_time_conversion_time_field_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Time Field Title'),
      '#description' => $this->t('Will serve as field title for input time field. Ex: Time.'),
      '#required' => TRUE,
      '#default_value' => $config->get('unix_time_conversion_time_field_title'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('unix_time_conversion.settings')
        ->set('unix_time_conversion_timestamp_field_title', $form_state->getValue('unix_time_conversion_timestamp_field_title'))
        ->set('unix_time_conversion_timestamp_field_description', $form_state->getValue('unix_time_conversion_timestamp_field_description'))
        ->set('unix_time_conversion_time_to_date_output_format', $form_state->getValue('unix_time_conversion_time_to_date_output_format'))
        ->set('unix_time_conversion_date_field_title', $form_state->getValue('unix_time_conversion_date_field_title'))
        ->set('unix_time_conversion_time_field_title', $form_state->getValue('unix_time_conversion_time_field_title'))
        ->save();

    return parent::submitForm($form, $form_state);
  }

}
